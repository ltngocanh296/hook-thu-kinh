import React, { Component } from 'react'
import { dataGlasses } from './dataJson'
export default class BaiTapThuKinh extends Component {
state={
    glasses:{ 
        id: 1,
        price: 30,
        name: "GUCCI G8850U",
        url: "./glassesImage/v3.png",
        desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",}
}

  renderGlassesList =()=>{
        return dataGlasses.map((glassesItem,index) => { 
            return <img onClick={()=>{this.handleChangeGlasses(glassesItem)}} className='ml-3 p-2 border border-width-1' style={{width:'100px',cursor:'pointer'}} key={index}src={glassesItem.url} />
        })
    }
    handleChangeGlasses=(newGlasses)=>{
        this.setState({
            glasses:newGlasses
        })
    }
  render() {
    return (
      <div style={{backgroundImage:'url(./glassesImage/background.jpg)',backgroundSize:'1500px',minHeight:'2000px'}}>
        <div style={{backgroundColor: 'rgba(0,0,0,.8)', minHeight:'2000px'}}>
            <h3 style={{backgroundColor: 'rgba(0,0,0,.3)'}} className='text-center text-light p-3'>LET'S TRY IT ^O^</h3>

            {/* modal thu kinh */}
            <div className="container">
            <div className="row mt-5 text-center">
                <div className="col-6">
                    <div className="position-relative">
                    <img className='position-absolute' style={{width:'250px'}} src="./glassesImage/model.jpg" alt='' />
                    <img style={{width:'150px', top:'75px', right: '70px', opacity:'0.5'}} className='position-absolute glassStyle' src ={this.state.glasses.url} />
                    </div>
                    <div style={{width:'250px', top:'200px',left:'270px', backgroundColor:'rgba(0,0,0,.3)', textAlign:'left', paddingLeft:'15px', height:'105px'}} className='position-relative'>
                        <p>{this.state.glasses.name}</p>
                        <p>{this.state.glasses.desc}</p>
                    </div>


                </div>
                <div className="col-6">
                    <img style={{width:'250px'}} src="./glassesImage/model.jpg" alt='' />
                </div>
            </div>
            </div>

            {/* data kinh */}
            <div className="bg-light container text-center mt-5 d-flex justify-content-center p-5">{this.renderGlassesList()}</div>
            

        </div>
      </div>
    )
  }
}
